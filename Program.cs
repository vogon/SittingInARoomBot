﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Speech.AudioFormat;
using System.Speech.Recognition;
using System.Speech.Synthesis;

namespace SittingInARoomBot
{
    public class Program
    {
        private const string LUCIER =
            "I am sitting in a room different from the one you are in now.  I am recording the " +
            "sound of my speaking voice and I am going to play it back into the room again and " +
            "again until the resonant frequencies of the room reinforce themselves so that any " +
            "semblance of my speech, with perhaps the exception of rhythm, is destroyed.  What " +
            "you will hear, then, are the natural resonant frequencies of the room articulated " +
            "by speech.  I regard this activity not so much as a demonstration of a physical " +
            "fact, but more as a way to smooth out any irregularities my speech might have.";

        public static void Main(string[] args)
        {
            string input, output = "";

            if (args.Length > 0) 
            {
                using (StreamReader f = new StreamReader(new FileStream(args[0], FileMode.Open)))
                {
                    input = f.ReadToEnd();
                }
            }
            else
            {
                input = LUCIER;
            }

            Random rng = new Random();
            SpeechAudioFormatInfo safi = new SpeechAudioFormatInfo(44100, AudioBitsPerSample.Sixteen, AudioChannel.Mono);

            // set up TTS and SR engines
            SpeechSynthesizer tts = new SpeechSynthesizer();

            IList<InstalledVoice> voices = tts.GetInstalledVoices().Where(v => v.Enabled).ToList();
            
            DictationGrammar g = new DictationGrammar();
            g.Enabled = true;

            SpeechRecognitionEngine srengine = new SpeechRecognitionEngine();
            srengine.LoadGrammar(g);

            while (true)
            {
                MemoryStream audio = new MemoryStream();

                // choose a random voice, for additional garbagetown
                int i = rng.Next(voices.Count);
                tts.SelectVoice(voices[i].VoiceInfo.Name);

                // synthesize input text
                tts.SetOutputToAudioStream(audio, safi);
                tts.Speak(input);
                audio.Seek(0, SeekOrigin.Begin);

                // recognize synthesized speech
                srengine.SetInputToAudioStream(audio, safi);

                while (true)
                {
                    RecognitionResult recoresult;

                    try
                    {
                        recoresult = srengine.Recognize();
                    }
                    catch (InvalidOperationException)
                    {
                        // ran out of audio input
                        break;
                    }

                    if (recoresult != null)
                    {
                        string snippet = recoresult.Text;
                        snippet = string.Format("{0}{1}.  ", Char.ToUpper(snippet[0]), snippet.Substring(1));

                        output += snippet;
                    }
                    else
                    {
                        continue;
                    }
                }

                Console.Out.WriteLine(output);
                Console.Out.WriteLine("==================");

                if (input == output) break;

                // move to the next step
                input = output;
                output = "";
            }
        }
    }
}
